package Ispit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OsnovneFunkcijePage {

	private WebDriver driver;
	
	public OsnovneFunkcijePage(WebDriver driver){
		this.driver = driver;
	}
	
	public WebElement getLanguageBtn(){
		return driver.findElement(By.xpath("//div/button[@class='btn btn-default dropdown-toggle']"));
	}
	
	public WebElement getLanguageSerCyr(){
		return driver.findElement(By.xpath("//div[@class='navbar-custom-menu']/ul/div/ul/li[2]"));
	}
	
	public WebElement getLanguageSerLat(){
		return driver.findElement(By.xpath("//div[@class='navbar-custom-menu']/ul/div/ul/li[1]"));
	}
	
	public WebElement getLanguageEng(){
		return driver.findElement(By.xpath("//div[@class='navbar-custom-menu']/ul/div/ul/li[3]"));
	}
	
	public WebElement getUserMenu(){
		return driver.findElement(By.xpath("//li[@class='dropdown user user-menu']"));
	}
	
	public WebElement getLogoutBtn(){
		return driver.findElement(By.xpath("//span[@translate='LOGOUT']"));
	}
	
	public WebElement getInstitutionBtn(){
		return driver.findElement(By.xpath("//li[@mns-sref='adminInstitution']"));
	}
	
	public WebElement getIstrazivaciBtn(){
		return driver.findElement(By.xpath("//li[@mns-sref='persons']"));
	}
	
	public WebElement getPodaciZaRegistar(){
		return driver.findElement(By.xpath("//tab-heading[contains(text(),'Podaci za registar')]"));
	}
	
	public WebElement getOsnovniPodaci(){
		return driver.findElement(By.xpath("//tab-heading[contains(text(),'Osnovni podaci')]"));
	}
	
	public WebElement getPodaciZaProjekte(){
		return driver.findElement(By.xpath("//tab-heading[contains(text(),'Podaci za projekte')]"));
	}
	
	public WebElement getUspesnaPoruka(){
		return driver.findElement(By.xpath("//h4[contains(text(),'SUCCESS')]"));
	}
}
