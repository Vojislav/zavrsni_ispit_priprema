package Ispit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPageIspit {

	private WebDriver driver;
	
	public LoginPageIspit (WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getUsername(){
		return driver.findElement(By.id("username"));
	}
	
	public void setUsername(String user){
		driver.findElement(By.id("username")).sendKeys(user);
	}
	
	public WebElement getPassword(){
		return driver.findElement(By.id("password"));
	}
	
	public void setPassword(String pass){
		WebElement password = getPassword();
		password.clear();
		password.sendKeys(pass);
	}
	
	public WebElement btnPrijava(){
		return driver.findElement(By.xpath("//button[contains(text(),'Prijavi se')]"));
	}
}
