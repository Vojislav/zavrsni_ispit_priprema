package Ispit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import rs.ac.uns.testdevelopment.ssluzba.helpers.Utils;

public class OsnovniPodaciPage {
	
	private WebDriver driver;
	
	public OsnovniPodaciPage(WebDriver driver){
		this.driver = driver;
	}

	public WebElement getNazivInstitucije(){
		//return driver.findElement(By.xpath("//input[@name='name']"));
		return Utils.waitForElementPresence(driver, (By.xpath("//input[@name='name']")), 10);
	}
	
	public void setNazivInstitucije(String naziv) {
		WebElement nazivInstitucije = getNazivInstitucije();
		nazivInstitucije.clear();
		nazivInstitucije.sendKeys(naziv);
	}
	
	public WebElement getNazivInstitucijeEng(){
		//return driver.findElement(By.xpath("//input[@name='name']"));
		return Utils.waitForElementPresence(driver, (By.xpath("//input[@name='eng_name']")), 10);
	}
	
	public void setNazivInstitucijeEng(String naziv) {
		WebElement nazivInstitucijeEng = getNazivInstitucijeEng();
		nazivInstitucijeEng.clear();
		nazivInstitucijeEng.sendKeys(naziv);
	}
	
	public Select getDrzava(){
		//return driver.findElement(By.xpath("//input[@name='name']"));
		return new Select(Utils.waitForElementPresence(driver, By.xpath("//select[@name='state']"), 10));
	}
	
	public void setDrzava(String drzava) {
		this.getDrzava().selectByVisibleText(drzava);
	}
	
	public WebElement getMesto(){
		return Utils.waitForElementPresence(driver, (By.xpath("//input[@name='place']")), 10);
	}
	
	public void setMesto(String naziv){
		WebElement grad = getMesto();
		grad.clear();
		grad.sendKeys(naziv);
	}
	
	public WebElement getOpstina(){
		return Utils.waitForElementPresence(driver, (By.xpath("//input[@name='townShipText']")), 10);
	}
	
	public void setOpstina(String naziv){
		WebElement opstina = getOpstina();
		opstina.clear();
		opstina.sendKeys(naziv);
	}
	
	public WebElement getUlicaIBroj(){
		return Utils.waitForElementPresence(driver, (By.xpath("//input[@name='address']")), 10);
	}
	
	public void setUlicaIBroj(String podaci){
		WebElement adresa = getUlicaIBroj();
		adresa.clear();
		adresa.sendKeys(podaci);
	}
	
	public WebElement getWeb(){
		return Utils.waitForElementPresence(driver, (By.xpath("//input[@name='uri']")), 10);
	}
	
	public void setWeb(String uri){
		WebElement webAdresa = getWeb();
		webAdresa.clear();
		webAdresa.sendKeys(uri);
	}
	
	public WebElement getEmail(){
		return Utils.waitForElementPresence(driver, (By.xpath("//input[@name='email']")), 10);
	}
	
	public void setEmail(String mail){
		WebElement email = getEmail();
		email.clear();
		email.sendKeys(mail);
	}
	
	public WebElement getPhone(){
		return Utils.waitForElementPresence(driver, (By.xpath("//input[@name='phone']")), 10);
	}
	
	public void setPhone(String broj){
		WebElement tel = getPhone();
		tel.clear();
		tel.sendKeys(broj);
	}
	
	public WebElement getSkraceniNaziv(){
		return Utils.waitForElementPresence(driver, (By.xpath("//input[@name='acro']")), 10);
	}
	
	public void setSkraceniNaziv(String naziv){
		WebElement skraceniNaziv = getSkraceniNaziv();
		skraceniNaziv.clear();
		skraceniNaziv.sendKeys(naziv);
	}
	
	public WebElement getNadredjenaInstBtn(){
		return Utils.waitForElementPresence(driver, (By.id("s2id_autogen1")), 10);
	}
	
	public WebElement getNadredjenaInst(){
		return Utils.waitForElementPresence(driver, (By.xpath("//li[@role='presentation']")), 10);
	}
	public void setNadredjenaInst(String inst){
		WebElement skraceniNaziv = getNadredjenaInst();
		skraceniNaziv.clear();
		skraceniNaziv.sendKeys(inst);
	}
	
	public WebElement getOdustani(){
		return Utils.waitForElementPresence(driver, By.xpath("//form[@name='Basic']//button[contains(text(),'Odustani')]"), 10);
	}
	
	public WebElement getSacuvaj(){
		return Utils.waitForElementPresence(driver, By.xpath("//form[@name='Basic']//button[contains(text(),'Sačuvaj')]"), 10);
	}
	
	public WebElement getPretragaNadredjenihInstitucija(){
		return Utils.waitForElementPresence(driver, (By.id("s2id_autogen2_search")), 10);
	}
	
	public void setPretragaNadredjenihInstitucija(String naziv){
		WebElement inst = getPretragaNadredjenihInstitucija();
		inst.clear();
		inst.sendKeys(naziv);
	}
}
