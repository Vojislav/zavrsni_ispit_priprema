package Ispit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import rs.ac.uns.testdevelopment.ssluzba.helpers.Utils;

public class PodaciZaRegistarPage {

	private WebDriver driver;
	
	public PodaciZaRegistarPage(WebDriver driver){
		this.driver = driver;
	}
	
	public WebElement getPIB (){
		return Utils.waitForElementPresence(driver, (By.xpath("//input[@name='pib']")), 10);
	}
	public void setPIB(String value){
		WebElement PIB = getPIB();
		PIB.clear();
		PIB.sendKeys(value);
	}
	
	public WebElement getMaticniBroj (){
		return Utils.waitForElementPresence(driver, (By.xpath("//input[@name='maticniBroj']")), 10);
	}
	public void setMaticniBroj(String value){
		WebElement maticniBroj = getMaticniBroj();
		maticniBroj.clear();
		maticniBroj.sendKeys(value);
	}
	
	public WebElement getBrojPoslednjeNaucneAkreditacije (){
		return Utils.waitForElementPresence(driver, (By.xpath("//input[@name='accreditationNumber']")), 10);
	}
	public void setBrojPoslednjeNaucneAkreditacije(String value){
		WebElement bPNA = getBrojPoslednjeNaucneAkreditacije();
		bPNA.clear();
		bPNA.sendKeys(value);
	}
	
	public WebElement getDatumPoslednjeNaucneAkreditacije (){
		return Utils.waitForElementPresence(driver, (By.xpath("//em-date-time-picker[@name='accreditationDate']//input")), 10);
	}
	public void setDatumPoslednjeNaucneAkreditacije(String value){
		WebElement element = getDatumPoslednjeNaucneAkreditacije();
		element.clear();
		element.sendKeys(value);
	}
	
	public WebElement getNazivInstitucijeIzAkreditacije (){
		return Utils.waitForElementPresence(driver, (By.xpath("//input[@name='accreditationNote']")), 10);
	}
	public void setNazivInstitucijeIzAkreditacije(String value){
		WebElement element = getNazivInstitucijeIzAkreditacije();
		element.clear();
		element.sendKeys(value);
	}
	
	public WebElement getNapomenaORegistru (){
		return Utils.waitForElementPresence(driver, (By.xpath("//input[@name='note']")), 10);
	}
	public void setNapomenaORegistru(String value){
		WebElement element = getNapomenaORegistru();
		element.clear();
		element.sendKeys(value);
	}
	
	public Select getVrstaInstitucije (){
		return new Select(Utils.waitForElementPresence(driver, (By.xpath("//select[@name='institutionType']")), 10));
	}
	public void setVrstaInstitucije(String value){
		this.getVrstaInstitucije().selectByVisibleText(value);
	}
	
	public WebElement getOsnovnaDelatnost (){
		return Utils.waitForElementPresence(driver, (By.id("s2id_autogen4")), 10);
	}
	public void setOsnovnaDelatnost(String value){
		WebElement element = getOsnovnaDelatnost();
		element.clear();
		element.sendKeys(value);
	}
	
	public WebElement getOsnivac (){
		return Utils.waitForElementPresence(driver, (By.xpath("//input[@name='founder']")), 10);
	}
	public void setOsnivac(String value){
		WebElement element = getOsnivac();
		element.clear();
		element.sendKeys(value);
	}
	
	public WebElement getDatumOsnivanja (){
		return Utils.waitForElementPresence(driver, (By.xpath("//em-date-time-picker[@name='date']//input")), 10);
	}
	public void setDatumOsnivanja(String value){
		WebElement element = getDatumOsnivanja();
		element.clear();
		element.sendKeys(value);
	}
	
	public WebElement getBrojResenjaOOsnivanju (){
		return Utils.waitForElementPresence(driver, (By.xpath("//input[@name='rescriptNumber']")), 10);
	}
	public void setBrojResenjaOOsnivanju(String value){
		WebElement element = getBrojResenjaOOsnivanju();
		element.clear();
		element.sendKeys(value);
	}
	
	public Select getVlasnickaStruktura (){
		return new Select(Utils.waitForElementPresence(driver, (By.xpath("//select[@name='ownershipStructure']")), 10));
	}
	public void setVlasnickaStruktura(String value){
		this.getVlasnickaStruktura().selectByVisibleText(value);
	}
	
	public WebElement getOdustani(){
		return Utils.waitForElementPresence(driver, By.xpath("//form[@name='Register']//button[contains(text(),'Odustani')]"), 10);
	}
	
	public WebElement getSacuvaj(){
		return Utils.waitForElementPresence(driver, By.xpath("//form[@name='Register']//button[contains(text(),'Sačuvaj')]"), 10);
	}
}
