package Ispit;

import static org.testng.AssertJUnit.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import rs.ac.uns.testdevelopment.ssluzba.pages.global.LoginPage;
import rs.ac.uns.testdevelopment.ssluzba.pages.global.MenuPage;
import rs.ac.uns.testdevelopment.ssluzba.pages.global.ModalDeletePage;
import rs.ac.uns.testdevelopment.ssluzba.pages.studenti.StudentsCreationPage;
import rs.ac.uns.testdevelopment.ssluzba.pages.studenti.StudentsListPage;

public class Tes2 {

	@Test
	public class Zadatak1 {
		private WebDriver driver;
		private LoginPageIspit loginPage;
		private String baseUrl;
		private OsnovneFunkcijePage oFpage;
		private OsnovniPodaciPage oPPage;
		private PodaciZaRegistarPage PZRP;
		
		@BeforeSuite
		public void setupSelenium() {
			baseUrl = "http://127.0.0.1:8080/#/login";
			driver = new FirefoxDriver();
			driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			driver.manage().window().setSize(new Dimension(1024, 768));
			driver.navigate().to(baseUrl);

		}
		@BeforeTest
		public void setupPages() {
			// init pages
			loginPage = new LoginPageIspit(driver);
			oFpage = new OsnovneFunkcijePage(driver);
			oPPage = new OsnovniPodaciPage(driver);
			PZRP = new PodaciZaRegistarPage(driver);
		}
		@Test
		public void login() {
			 loginPage.setUsername("djura@djuraminis.com");
			 loginPage.setPassword("adminvinca");
			 loginPage.btnPrijava().click();
			 oFpage.getLanguageBtn().click();
			 oFpage.getLanguageSerLat().click();
			// oFpage.getLanguageBtn().click();
			 //oFpage.getLanguageSerLat().click();
			 //oFpage.getUserMenu().click();
			 //oFpage.getLogoutBtn().click();
			// oFpage.getIstrazivaciBtn().click();
			 //oFpage.getInstitutionBtn().click();
			 oPPage.setNazivInstitucije("Naziv");
			 oPPage.setNazivInstitucijeEng("Name");
			 oPPage.setDrzava("Srbija");
			 oPPage.setMesto("Novi Sad");
			 oPPage.setOpstina("Podbara");
			 oPPage.setUlicaIBroj("Bgd kej 37");
			 oPPage.setWeb("www.nesto.com");
			 oPPage.setEmail("vojans1992@gmail.com");
			 oPPage.setPhone("063/769-44-17");
			 oPPage.setSkraceniNaziv("nzv");
			 oPPage.getNadredjenaInstBtn().click();
			 oPPage.setPretragaNadredjenihInstitucija("Fak");
			 oPPage.getNadredjenaInst().click();
			 oPPage.getSacuvaj().click();
			 Assert.assertTrue(oFpage.getUspesnaPoruka().isDisplayed());
			 oFpage.getPodaciZaRegistar().click();
			 PZRP.setPIB("otkud znam");
			 PZRP.setMaticniBroj("54646");
			 PZRP.setBrojPoslednjeNaucneAkreditacije("neki broj");
			 PZRP.setDatumPoslednjeNaucneAkreditacije("12.12.2012");
			 PZRP.setNazivInstitucijeIzAkreditacije("neki naziv");
			 PZRP.setNapomenaORegistru("neka napomena");
			 PZRP.setVrstaInstitucije("Fakultet");
			 PZRP.setOsnivac("JA SAM OSNIVAC");
			 PZRP.setDatumOsnivanja("11.11.2011");
			 PZRP.setBrojResenjaOOsnivanju("opet neki broj");
			 PZRP.setVlasnickaStruktura("Privatna");
			 Assert.assertTrue(oFpage.getUspesnaPoruka().isDisplayed());
			 String test;
			 test = PZRP.getMaticniBroj().getAttribute("value");
			 Assert.assertEquals(test,"54646");
			 PZRP.getSacuvaj().click();
		}
		@AfterSuite
		public void afterTest(){
			driver.close();
		}
	}
}
