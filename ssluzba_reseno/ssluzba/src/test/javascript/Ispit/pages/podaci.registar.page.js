var PodaciRegistarPage = function() {};
var utils = require('./utils.js');

PodaciRegistarPage.prototype = Object.create({}, {

    pIB: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//input[@name='pib']"), 10000);
        },
        set: function(value) {
            this.pIB.clear();
            this.pIB.sendKeys(value);
        }
    },

    maticniBroj: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//input[@name='maticniBroj']"), 10000);
        },
        set: function(value) {
            this.maticniBroj.clear();
            this.maticniBroj.sendKeys(value);
        }
    },

    brojPoslednjeNaucneAkreditacije: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//input[@name='accreditationNumber']"), 10000);
        },
        set: function(value) {
            this.brojPoslednjeNaucneAkreditacije.clear();
            this.brojPoslednjeNaucneAkreditacije.sendKeys(value);
        }
    },

    datumPoslednjeNaucneAkreditacije: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//em-date-time-picker[@name='accreditationDate']//input"), 10000);
        },
        set: function(value) {
            this.datumPoslednjeNaucneAkreditacije.clear();
            this.datumPoslednjeNaucneAkreditacije.sendKeys(value);
        }
    },

    nazivInstirtucijeIzAkreditacije: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//input[@name='accreditationNote']"), 10000);
        },
        set: function(value) {
            this.nazivInstirtucijeIzAkreditacije.clear();
            this.nazivInstirtucijeIzAkreditacije.sendKeys(value);
        }
    },

    napomenaORegistru: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//input[@name='note']"), 10000);
        },
        set: function(value) {
            this.napomenaORegistru.clear();
            this.napomenaORegistru.sendKeys(value);
        }
    },

    vrstaInstitucije: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//select[@name='institutionType']"), 10000);
        },
        set: function(value) {
            this.vrstaInstitucije.element(by.xpath("//option[contains(text(),'" + value + "')]")).click();
        }
    },

    osnovnaDelatnost: {
        get: function() {
            return utils.waitForElementPresence(by.id("s2id_autogen4"), 10000);
        },
        set: function(value) {
            this.osnovnaDelatnos.clear();
            this.osnovnaDelatnos.sendKeys(value);
        }
    },

    osnivac: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//input[@name='founder']"), 10000);
        },
        set: function(value) {
            this.osnivac.clear();
            this.osnivac.sendKeys(value);
        }
    },

    datumOsnivanja: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//em-date-time-picker[@name='date']//input"), 10000);
        },
        set: function(value) {
            this.datumOsnivanja.clear();
            this.datumOsnivanja.sendKeys(value);
        }
    },

    brojResenjaOOsnivanju: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//input[@name='rescriptNumber']"), 10000);
        },
        set: function(value) {
            this.brojResenjaOOsnivanju.clear();
            this.brojResenjaOOsnivanju.sendKeys(value);
        }
    },

    vlasnickaStruktura: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//select[@name='ownershipStructure']"), 10000);
        },
        set: function(value) {
            this.vlasnickaStruktura.element(by.xpath("//option[contains(text(),'" + value + "')]")).click();
        }
    },

    sacuvaj: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//form[@name='Register']//button[contains(text(),'Sačuvaj')]"), 10000);
        }
    },

    odustani: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//form[@name='Register']//button[contains(text(),'Odustani')]"), 10000);
        }
    }
});
module.exports = PodaciRegistarPage;