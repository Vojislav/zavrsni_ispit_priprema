var OsnovePage = function() {};
var utils = require('./utils.js');

OsnovePage.prototype = Object.create({}, {

    languageBtn: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//div/button[@class='btn btn-default dropdown-toggle']"));
        }
    },

    serbianCyr: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//div[@class='navbar-custom-menu']/ul/div/ul/li[2]"));
        }
    },

    serbianLat: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//div[@class='navbar-custom-menu']/ul/div/ul/li[1]"));
        }
    },

    english: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//div[@class='navbar-custom-menu']/ul/div/ul/li[3]"));
        }
    },

    userMenu: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//li[@class='dropdown user user-menu']"));
        }
    },

    logoutBtn: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//span[@translate='LOGOUT']"));
        }
    },

    institutionBtn: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//li[@mns-sref='adminInstitution']"));
        }
    },

    istrazivaciBtn: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//li[@mns-sref='persons']"));
        }
    },

    osnovniPodaci: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//tab-heading[contains(text(),'Osnovni podaci')]"));
        }
    },

    podaciZaRegistar: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//tab-heading[contains(text(),'Podaci za registar')]"));
        }
    },

    podaciZaProjekte: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//tab-heading[contains(text(),'Podaci za projekte')]"));
        }
    },

    uspesnaPoruka: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//h4[contains(text(),'SUCCESS')]"));
        }
    },
});
module.exports = OsnovePage;