var PodaciPage = function() {};
var utils = require('./utils.js');

PodaciPage.prototype = Object.create({}, {

    nazivInstitucije: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//input[@name='name']"), 10000);
        },
        set: function(value) {
            this.nazivInstitucije.clear();
            this.nazivInstitucije.sendKeys(value);
        }
    },
    nazivInstitucijeEng: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//input[@name='eng_name']"), 10000);
        },
        set: function(value) {
            this.nazivInstitucijeEng.clear();
            this.nazivInstitucijeEng.sendKeys(value);
        }
    },
    drzava: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//select[@name='state']"), 10000);
        },
        set: function(value) {
            this.drzava.element(by.xpath("//option[contains(text(),'Srbija')]")).click();
        }
    },
    mesto: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//input[@name='place']"), 10000);
        },
        set: function(value) {
            this.mesto.clear();
            this.mesto.sendKeys(value);
        }
    },
    opstina: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//input[@name='townShipText']"), 10000);
        },
        set: function(value) {
            this.opstina.clear();
            this.opstina.sendKeys(value);
        }
    },
    adresa: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//input[@name='address']"), 10000);
        },
        set: function(value) {
            this.adresa.clear();
            this.adresa.sendKeys(value);
        }
    },
    web: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//input[@name='uri']"), 10000);
        },
        set: function(value) {
            this.web.clear();
            this.web.sendKeys(value);
        }
    },
    mail: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//input[@name='email']"), 10000);
        },
        set: function(value) {
            this.mail.clear();
            this.mail.sendKeys(value);
        }
    },
    phone: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//input[@name='phone']"), 10000);
        },
        set: function(value) {
            this.phone.clear();
            this.phone.sendKeys(value);
        }
    },
    skraceniNaziv: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//input[@name='acro']"), 10000);
        },
        set: function(value) {
            this.skraceniNaziv.clear();
            this.skraceniNaziv.sendKeys(value);
        }
    },
    nadredjenaInstBtn: {
        get: function() {
            return utils.waitForElementPresence(by.id("s2id_autogen1"), 10000);
        }
    },
    nadredjenaInst: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//li[@role='presentation']"), 10000);
        }
    },
    nadredjenaInstPretraga: {
        get: function(){
            return utils.waitForElementPresence(by.id("s2id_autogen2_search"),10000);
        },
        set: function(value) {
            this.nadredjenaInstPretraga.clear();
            this.nadredjenaInstPretraga.sendKeys(value);
        }
    },
    odustani: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//form[@name='Basic']//button[contains(text(),'Odustani')]"),10000);
        }
    },
    sacuvaj: {
        get: function() {
            return utils.waitForElementPresence(by.xpath("//form[@name='Basic']//button[contains(text(),'Sačuvaj')]"),10000);
        }
    },
    signInBtn: {
        get: function() {
            return utils.waitForElementPresence(by.buttonText('Prijavi se'),10000);
        }
    }
});
module.exports = PodaciPage;