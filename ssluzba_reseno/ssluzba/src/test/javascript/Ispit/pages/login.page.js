var LoginPage = function() {};
var utils = require('./utils.js');

LoginPage.prototype = Object.create({}, {

    username: {
        get: function() {
            return utils.waitForElementPresence(by.id('username'), 10000);
        },
        set: function(value) {
            this.username.clear();
            this.username.sendKeys(value);
        }
    },
    password: {
        get: function() {
            return utils.waitForElementPresence(by.id('password'), 10000);
        },
        set: function(value) {
            this.password.clear();
            this.password.sendKeys(value);
        }
    },
    signInBtn: {
        get: function() {
            return utils.waitForElementPresence(by.buttonText('Prijavi se'),10000);
        }
    }
});
module.exports = LoginPage;